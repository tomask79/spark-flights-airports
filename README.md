# Apache Spark and task from Hortonworks workshop #

    %spark2
    // most common routes = result_format (origin_city, destination_city, count)

    case class airport (id: Int, city: String);
    case class flight (origin_id: Int, destination_id: Int)

    var airportRDD = sc.textFile("/tests/airports/airports.csv");
    var flightsRDD = sc.textFile("/tests/flights/flights.csv");

    var airportDF = airportRDD.map(line=>new airport(line.split(",")(0).toInt, line.split(",")(2))).toDF();

    airportDF.createOrReplaceTempView("airports");

    var flightsDF = flightsRDD.map(line=>new flight(line.split(",")(1).trim().toInt, line.split(",")(2).trim().toInt)).toDF();

    flightsDF.createOrReplaceTempView("flights");

    var sortedDF = sqlContext.sql("select origin_id, destination_id, count(*) as pocet from flights group by origin_id, destination_id order by pocet desc");

    sortedDF.createOrReplaceTempView("sortedFlights");

    var resultDF1 = sqlContext.sql("select a.city, b.city, c.pocet from sortedFlights c INNER JOIN airports a ON a.id = c.origin_id "+
                                                                    "               INNER JOIN airports b ON b.id = c.destination_id order by pocet desc "     
    );

    resultDF1.show();

Zeppelin output:

    defined class airport
    defined class flight
    airportRDD: org.apache.spark.rdd.RDD[String] = /tests/airports/airports.csv MapPartitionsRDD[1434] at textFile at <console>:72
    flightsRDD: org.apache.spark.rdd.RDD[String] = /tests/flights/flights.csv MapPartitionsRDD[1436] at textFile at <console>:72
    airportDF: org.apache.spark.sql.DataFrame = [id: int, city: string]
    flightsDF: org.apache.spark.sql.DataFrame = [origin_id: int, destination_id: int]
    sortedDF: org.apache.spark.sql.DataFrame = [origin_id: int, destination_id: int ... 1 more field]
    resultDF1: org.apache.spark.sql.DataFrame = [city: string, city: string ... 1 more field]
    +--------+------------+-----+
    |    city|        city|pocet|
    +--------+------------+-----+
    | NewYork|      Dallas|    3|
    |  Dallas| Los angeles|    2|
    |  Dallas|      Madrid|    1|
    +--------+------------+-----+




